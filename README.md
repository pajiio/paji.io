# README #

how does the game work?

The w, a, s, d keys control movement. w for up, s for down, d for right, a for left.

There is a reset button at the bottom of the screen so you the program does not have to be relaunched.

Every 10 points the crates will start to move faster until you reach 40 points. Beyond that is impossible.